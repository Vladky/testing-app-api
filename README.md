# API для тестового задания

## Задание
Реализовать небольшое приложение, реализующее вывод карточек событий на странице.
Карточки должны быть адаптивными, выводиться сеткой и подгружаться по мере достижения конца страницы / нажатию на кнопку / переключении пагинации (на свободный выбор)

Данные о событиях предоставляет API (данный проект). Логику поиска, фильтрации, открытия карточек реализовывать не нужно. Стоит учесть, что API может эмулировать ошибку (вероятность появления ошибок управляется входным параметром сервера).

Оцениваться будет верстка, подход к архитектуре (переусложнять, разрабатывать UI-киты и т.д. не нужно, но стоит думать о том, что потенциально проект мог бы масштабироваться), подход к написанию кода и использованию фреймворков, а также работа с асинхронными запросами и обработке ошибок.

Ожидаемое время выполнения: 1.5–2 часа.

## Ссылка на макет

https://www.figma.com/file/aOj5UZYkMbnDC9yTeSyqbb/Testing-app-layout?node-id=0%3A1

## Настройка и запуск
```
npm i
node server.js --error-probability=<вероятность_возникновения_ошибок(в_процентах)>
```
## Документация по API

Base URLs:

- <a href="https://virtserver.swaggerhub.com/Vladky/testing-app-api/1.0.0">https://virtserver.swaggerhub.com/Vladky/testing-app-api/1.0.0</a>

Email: <a href="mailto:vkorolev@kassir.ru">Support</a>

`GET /`

_Возвращает список событий_

<h3 id="get__-parameters">Parameters</h3>

| Name | In    | Type   | Required | Description             |
| ---- | ----- | ------ | -------- | ----------------------- |
| page | query | number | false    | Содержит номер страницы |

> Example responses

> 200 Response

```json
[
  {
    "id": 1,
    "title": "Altenwerth Group",
    "venue": "Port Xandershire",
    "poster": "https://source.unsplash.com/400x300?musician",
    "date": "2016-08-29T09:12:33.001Z",
    "price": 1000
  }
]
```

<h3 id="get__-responses">Responses</h3>

| Status | Meaning                                                                    | Description                                 | Schema |
| ------ | -------------------------------------------------------------------------- | ------------------------------------------- | ------ |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)                    | Найдены результаты, удовлетворяющие запросу | Inline |
| 500    | [Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1) | Неизвестная ошибка                          | Inline |

<h3 id="get__-responseschema">Response Schema</h3>

Status Code **200**

| Name        | Type                    | Required | Restrictions | Description      |
| ----------- | ----------------------- | -------- | ------------ | ---------------- |
| _anonymous_ | [[Event](#schemaevent)] | false    | none         | none             |
| » id        | number                  | false    | none         | ID события       |
| » title     | string                  | false    | none         | Заголовок        |
| » venue     | string                  | false    | none         | Место проведения |
| » poster    | string                  | false    | none         | Ссылка на постер |
| » date      | string(datetime)        | false    | none         | Дата проведения  |
| » price     | number                  | false    | none         | Стоимость        |

Status Code **500**

| Name      | Type   | Required | Restrictions | Description         |
| --------- | ------ | -------- | ------------ | ------------------- |
| » message | string | false    | none         | Сообщение об ошибке |

<aside class="success">
This operation does not require authentication
</aside>

# Schemas

<h2 id="tocS_Event">Event</h2>
<!-- backwards compatibility -->
<a id="schemaevent"></a>
<a id="schema_Event"></a>
<a id="tocSevent"></a>
<a id="tocsevent"></a>

```json
{
  "id": 1,
  "title": "Altenwerth Group",
  "venue": "Port Xandershire",
  "poster": "https://source.unsplash.com/400x300?musician",
  "date": "2016-08-29T09:12:33.001Z",
  "price": 1000
}
```

### Properties

| Name   | Type             | Required | Restrictions | Description      |
| ------ | ---------------- | -------- | ------------ | ---------------- |
| id     | number           | false    | none         | ID события       |
| title  | string           | false    | none         | Заголовок        |
| venue  | string           | false    | none         | Место проведения |
| poster | string           | false    | none         | Ссылка на постер |
| date   | string(datetime) | false    | none         | Дата проведения  |
| price  | number           | false    | none         | Стоимость        |

<h2 id="tocS_Error">Error</h2>
<!-- backwards compatibility -->
<a id="schemaerror"></a>
<a id="schema_Error"></a>
<a id="tocSerror"></a>
<a id="tocserror"></a>

```json
{
  "message": "Что-то сломалось"
}
```

### Properties

| Name    | Type   | Required | Restrictions | Description         |
| ------- | ------ | -------- | ------------ | ------------------- |
| message | string | false    | none         | Сообщение об ошибке |
