const express = require("express");
const app = express();
const port = 3000;
const { faker } = require("@faker-js/faker");
const minimist = require("minimist");
const ARGS = minimist(process.argv.slice(2));

const generateMocks = (count, start = 1) => {
  return new Array(count).fill().map((_, i) => ({
    id: parseInt(count * (start - 1)) + parseInt(i) + 1,
    title: faker.company.companyName(),
    venue: faker.address.city(),
    poster: faker.image.unsplash.image(400, 300, "musician"),
    date: faker.date.soon(30),
    price: parseInt(Math.random() * 50 + 5) * 100,
  }));
};

app.get("/", (req, res) => {
  const { page } = req.query;
  const count = 24;
  const data = generateMocks(count, page);
  const delay = Math.random() * 3000;
  const isError = Math.random() * 100 <= ARGS["error-probability"];
  setTimeout(() => {
    if (isError) {
      res.status(500).send({ message: "Что-то сломалось" });
    } else {
      res.send(data);
    }
  }, delay);
});

app.listen(port, () =>
  console.log(`Testing app API listening on port ${port}!`)
);
